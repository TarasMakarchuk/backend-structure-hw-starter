const knex = require("knex");
const dbConfig = require('../config/db.config.knex');

const database = knex(dbConfig.development);

module.exports = database;