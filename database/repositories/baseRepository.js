const knex = require("knex");
const dbConfig = require('../../config/db.config.knex');

const database = knex(dbConfig.development);


// const { dbAdapter } = require('../../config/db.config.knex');
const { v4 } = require('uuid');

class BaseRepository {
    constructor(collectionName) {
        this.dbContext = database.raw(collectionName);
        this.collectionName = collectionName;
    }

    generateId() {
        return v4();
    }

    getAll() {
        return this.dbContext.value();
    }

    getOneById(id) {
        return this.dbContext.find({ id }).value();
    }

    getOne(search) {
        return this.dbContext.find(search).value();
    }

    create(data) {
        data.id = this.generateId();
        data.createdAt = new Date();
        const list = this.dbContext.push(data).write();
        return list.find(it => it.id === data.id);
    }

    update(id, dataToUpdate) {
        dataToUpdate.updatedAt = new Date();
        return this.dbContext.find({ id }).assign(dataToUpdate).write();
    }

    delete(id) {
        return this.dbContext.remove({ id }).write();
    }
}

exports.BaseRepository = BaseRepository;