const { BaseRepository } = require('./baseRepository');

class UserRepository extends BaseRepository {
	constructor() {
		super('user');
	}
}

exports.UserRepository = new UserRepository();