const { BaseRepository } = require('./baseRepository');

class TransactionRepository extends BaseRepository {
	constructor() {
		super('transaction');
	}
}

exports.TransactionRepository = new TransactionRepository();