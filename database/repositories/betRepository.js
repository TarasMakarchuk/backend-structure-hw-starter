const { BaseRepository } = require('./baseRepository');

class BetRepository extends BaseRepository {
	constructor() {
		super('bet');
	}
}

exports.BetRepository = new BetRepository();