const joi = require('joi');
const {Schema} = require('../schema');

class UserGetSchema extends Schema {
	constructor(data) {
		super();
		this.data = data;
		this.schema = joi.object({
			id: joi.string().uuid(),
		}).required();
	}
}

module.exports = new UserGetSchema();