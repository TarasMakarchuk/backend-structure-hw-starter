const joi = require('joi');
const {Schema} = require('../schema');

class EventPutSchema extends Schema {
	constructor(data) {
		super();
		this.data = data;
		this.schema = joi.object({
			score: joi.string().required(),
		}).required();
	}
}

module.exports = new EventPutSchema();