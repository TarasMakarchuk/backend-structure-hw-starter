const authRoutes = require('./authRoutes');
const userRoutes = require('./userRoutes');
const eventsRoutes = require('./eventRoutes');
const transactionRoutes = require('./transactionRoutes');
const betsRoutes = require('./betRoutes');
const statsRoutes = require('./statsRoutes');

module.exports = (app) => {
	app.use('/auth', authRoutes);
	app.use('/users', userRoutes);
	app.use('/transactions', transactionRoutes);
	app.use('/events', eventsRoutes);
	app.use('/bets', betsRoutes);
	app.use('/stats', statsRoutes);
};