const { Router } = require('express');
const jwt = require("jsonwebtoken");
const Stats = require('../../database/stats');

const router = Router();

router.get("/", (req, res) => {
	try {
		let token = req.headers.authorization;
		if(!token) {
			return res.status(401).send({ error: 'Not Authorized' });
		}
		token = token.replace('Bearer ', '');
		try {
			const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
			if (tokenPayload.type !== 'admin') {
				return res.status(403).send({ error: 'Forbidden' });
			}
		} catch (err) {
			return res.status(401).send({ error: 'Not Authorized' });
		}
		res.send(Stats.getStats());
	} catch (err) {
		return res.status(500).send("Internal Server Error");
	}
});

module.exports = router;