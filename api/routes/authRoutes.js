const { Router } = require('express');
const AuthService = require('../services/authService_');

const router = Router();

router.post('/', (req, res, next) => {
	try {
		AuthService.login(req.headers.authorization);
		res.status(200).send(res.data);
		// res.data = data;
	} catch (err) {
		res.err = err;
	} finally {
		next();
	}
});

module.exports = router;