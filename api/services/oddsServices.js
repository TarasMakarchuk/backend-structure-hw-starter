const { OddsRepository } = require('../../database/repositories/oddsRepository');

class OddsServices {

	getAllOddsData = () => OddsRepository.getAll();

	getOddsById = id => {
		if (id) {
			return OddsRepository.getOneById(id);
		} else {
			return null;
		}
	};

	saveOddsData = data => {
		if (data) {
			return OddsRepository.create(data);
		} else {
			return null;
		}
	};

	updateOddsData = (id, data) => {
		if (id && data) {
			return OddsRepository.update(id, data);
		} else {
			return null;
		}
	};

	deleteOdds = id => {
		if (id) {
			return OddsRepository.delete(id);
		} else {
			return null;
		}
	};
}

module.exports = new OddsServices();