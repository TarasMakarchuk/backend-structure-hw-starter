const UserService = require('./userService_');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();