const { TransactionRepository } = require('../../database/repositories/transactionRepository');

class TransactionServices {

	getAllTransactionsData = () => TransactionRepository.getAll();

	getTransactionById = id => {
		if (id) {
			return TransactionRepository.getOneById(id);
		} else {
			return null;
		}
	};

	saveTransactionData = data => {
		if (data) {
			return TransactionRepository.create(data);
		} else {
			return null;
		}
	};

	updateTransactionData = (id, data) => {
		if (id && data) {
			return TransactionRepository.update(id, data);
		} else {
			return null;
		}
	};

	deleteTransaction = id => {
		if (id) {
			return TransactionRepository.delete(id);
		} else {
			return null;
		}
	};
}

module.exports = new TransactionServices();