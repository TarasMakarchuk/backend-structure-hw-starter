const { BetRepository } = require('../../database/repositories/betRepository');

class BetServices {

	getAllBetsData = () => BetRepository.getAll();

	getBetById = id => {
		if (id) {
			return BetRepository.getOneById(id);
		} else {
			return null;
		}
	};

	saveBetData = data => {
		if (data) {
			return BetRepository.create(data);
		} else {
			return null;
		}
	};

	updateBetData = (id, data) => {
		if (id && data) {
			return BetRepository.update(id, data);
		} else {
			return null;
		}
	};

	deleteBet = id => {
		if (id) {
			return BetRepository.delete(id);
		} else {
			return null;
		}
	};
}

module.exports = new BetServices();