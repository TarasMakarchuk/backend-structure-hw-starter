const { EventRepository } = require('../../database/repositories/eventRepository');

class EventServices {

	getAllEventsData = () => EventRepository.getAll();

	getEventById = id => {
		if (id) {
			return EventRepository.getOneById(id);
		} else {
			return null;
		}
	};

	saveEventData = data => {
		if (data) {
			return EventRepository.create(data);
		} else {
			return null;
		}
	};

	updateEventData = (id, data) => {
		if (id && data) {
			return EventRepository.update(id, data);
		} else {
			return null;
		}
	};

	deleteEvent = id => {
		if (id) {
			return EventRepository.delete(id);
		} else {
			return null;
		}
	};
}

module.exports = new EventServices();