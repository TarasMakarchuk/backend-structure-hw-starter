const { UserRepository } = require('../../database/repositories/userRepository');

class UserService {

	getAllUsersData = () => UserRepository.getAll();

	getUserById = id => {
		if (id) {
			return UserRepository.getOneById(id);
		} else {
			return null;
		}
	};

	saveUserData = data => {
		if (data) {
			return UserRepository.create(data);
		} else {
			return null;
		}
	};

	updateUserData = (id, data) => {
		if (data) {
			return UserRepository.update(id, data);
		} else {
			return null;
		}
	};

	deleteUser = id => {
		if (id) {
			return UserRepository.delete(id);
		} else {
			return null;
		}
	};

	search(search) {
		const item = UserRepository.getOne(search);
		if(!item) {
			return null;
		}
		return item;
	}
}

module.exports = new UserService();