const {user} = require('../../database/models/user');
const {validateData, validateFields} = require('../middlewares/validation.middleware');

const emailPattern = new RegExp(/([a-zA-Z0-9]+)([.{1}])?([a-zA-Z0-9]+)@gmail([.])com/g);
const phoneNumberPattern = new RegExp(/^\+380\d{9}$/);
const MIN_NAME_LENGTH = 2;
const MIN_PASSWORD_LENGTH = 3;

const userValidate = ((req, res, next) => {
	const {type, name, email, phone, city, password} = req.body;

	validateData(req, res);
	validateFields(req, res, user);

	if (!type) {
		res.status(400).send({
			error: true,
			message: `Type is required`,
		});
		return false;
	} else if (type !== 'client' || type !== 'admin') {
		res.status(400).send({
			error: true,
			message: `Type should be admin or client`,
		});
		return false;
	}

	if (!name) {
		res.status(400).send({
			error: true,
			message: `Name is required`,
		});
		return false;
	} else if (name.length < MIN_NAME_LENGTH) {
		res.status(400).send({
			error: true,
			message: `Name should be less than ${MIN_NAME_LENGTH} symbols`,
		});
		return false;
	}

	if (!email) {
		res.status(400).send({
			error: true,
			message: `Email is required`,
		});
		return false;
	} else if (!email.match(emailPattern)) {
		res.status(400).send({
			error: true,
			message: `Email ${email} should be registered on gmail.com`,
		});
		return false;
	}

	if (!phone) {
		res.status(400).send({
			error: true,
			message: `Phone number is required`,
		});
		return false;
	} else if (!phone.match(phoneNumberPattern)) {
		res.status(400).send({
			error: true,
			message: `Phone number ${phone} should be in the format +380xxxxxxxxx`,
		});
		return false;
	}

	if (!password) {
		res.status(400).send({
			error: true,
			message: `Password is required`,
		});
		return false;
	} else if (password.length < MIN_PASSWORD_LENGTH) {
		res.status(400).send({
			error: true,
			message: `Password length should be more that ${MIN_PASSWORD_LENGTH} symbols`,
		});
		return false;
	}

	if (!city) {
		res.status(400).send({
			error: true,
			message: `City is required`,
		});
		return false;
	}

	return true;
});

const createUserValid = (req, res, next) => {
	const valid = userValidate(req, res, next);
	next();
	return valid;
}

module.exports = createUserValid;