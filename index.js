const express = require("express");
const ee = require('events');
const winston = require('winston');
const expressWinston = require('express-winston');
const database = require('./database/connection');
const Stats = require('./database/stats');

const app = express();
app.use(express.json());
const port = process.env.PORT;

app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console()
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
  ),
  meta: false,
  msg: "HTTP  ",
  expressFormat: true,
  colorize: false,
  ignoreRoute: function (req, res) { return false; }
}));

const statEmitter = new ee();

// user eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImVkODA3YTBkLWU4MjQtNDIzOC1iYzU5LTdlOGFiNmE5NTk1YyIsInR5cGUiOiJjbGllbnQiLCJpYXQiOjE2MjM2NjgyMjB9.pDmW_3gQLi9QEU3FAjN6aV_XlxTc2T8taIBatm_prVU
// admin eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjRjM2EwM2ZjLTk5N2YtNGJhZS05ZjQ2LTYyMzUwOTFlNDM5YSIsInR5cGUiOiJhZG1pbiIsImlhdCI6MTYyMzU5NTQ1NX0.Nz7UrgMwhbviVO72YurU1vGWUbw_1bEBe0sC2y680Y8

app.use((req, res, next) => {
  database.raw('select 1+1 as result')
    .then(() => next())
    .catch(error => {
      console.error('Unable to connect to the database', error);
    });
});


const routes = require('./api/routes/index');
routes(app);

app.get('/health', (req, res) => {
  res.send('Hello World!');
});

app.listen(port, () => {
  statEmitter.on('newUser', () => {
    Stats.setTotalUser(1);
  });
  statEmitter.on('newBet', () => {
    Stats.setTotalBets(1);
  });
  statEmitter.on('newEvent', () => {
    Stats.setTotalEvents(1);
  });

  console.log(`App listening at http://localhost:${port}`);
});

module.exports = { app };